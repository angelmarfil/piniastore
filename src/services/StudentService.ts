import type { IStudent } from '@/interfaces/IStudent'
import { ref, type Ref } from 'vue'

const endpoint: string = import.meta.env.VITE_API_URL

export default class StudentService {
  private student: Ref<IStudent>
  private students: Ref<IStudent[]>

  constructor() {
    this.student = ref({
      id: '',
      name: '',
      email: '',
      group: ''
    }) as Ref<IStudent>
    this.students = ref([])
  }

  getStudent() {
    return this.student
  }

  getStudents() {
    return this.students
  }

  async fetchStudents() {
    try {
      const response = await fetch(endpoint)
      this.students.value = await response.json()
      return this.students.value
    } catch (error) {
      console.error(error)
    }
  }

  async fetchStudent(id: string) {
    try {
      const response = await fetch(endpoint + '/' + id)
      this.student.value = await response.json()
      return this.student.value
    } catch (error) {
      console.error(error)
    }
  }

  async createStudent(student: any) {
    try {
      await fetch(endpoint, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(student)
      })
    } catch (error) {
      console.error(error)
    }
  }

  async putStudent(student: any) {
    try {
      await fetch(endpoint + `/?id=${student.id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(student)
      })
    } catch (error) {
      console.error(error)
    }
  }

  async deleteStudent(id: string) {
    try {
      await fetch(endpoint + `/${id}`, {
        method: 'DELETE'
      })
    } catch (error) {
      console.error(error)
    }
  }
}
