import type { IStudent } from '@/interfaces/IStudent'
import StudentService from '@/services/StudentService'
import { defineStore } from 'pinia'
import { computed, ref, type Ref } from 'vue'

export const useStudentStore = defineStore('student', () => {
  // state
  const student = ref({ id: '', name: '', email: '', group: '' }) as Ref<IStudent>
  const students: Ref<IStudent[]> = ref([])
  const service = ref()
  const alert = ref({
    type: '',
    text: ''
  })

  //actions
  async function getStudents(): Promise<void> {
    try {
      service.value = new StudentService()
      students.value = await service.value.fetchStudents()
    } catch (error) {
      console.error(error)
    }
  }

  async function getStudent(id: string): Promise<void> {
    try {
      service.value = new StudentService()
      student.value = await service.value.fetchStudent(id)
    } catch (error) {
      console.error(error)
    }
  }

  async function createStudent(student: any) {
    try {
      service.value = new StudentService()
      await service.value.createStudent(student)
      await getStudents()
    } catch (error) {
      console.error(error)
    }
  }

  async function putStudent(student: any) {
    try {
      service.value = new StudentService()
      await service.value.putStudent(student)
      await service.value.fetchStudents()
    } catch (error) {
      console.error(error)
    }
  }

  async function deleteStudent(id: string) {
    try {
      service.value = new StudentService()
      await service.value.deleteStudent(id)
      students.value = students.value.filter((student) => student.id !== id)
    } catch (error) {
      console.error(error)
    }
  }

  //getters
  const totalStudents = computed(() => students.value.length)

  const IDYGS81 = computed(
    () => students.value.filter((student) => student.group === 'IDYGS81').length
  )

  const IDYGS82 = computed(
    () => students.value.filter((student) => student.group === 'IDYGS82').length
  )

  const IDYGS83 = computed(
    () => students.value.filter((student) => student.group === 'IDYGS83').length
  )

  const others = computed(
    () =>
      students.value.filter(
        (student) =>
          student.group !== 'IDYGS81' && student.group !== 'IDYGS82' && student.group !== 'IDYGS83'
      ).length
  )

  return {
    student,
    students,
    alert,
    getStudent,
    getStudents,
    createStudent,
    putStudent,
    deleteStudent,
    IDYGS81,
    IDYGS82,
    IDYGS83,
    others,
    totalStudents
  }
})
