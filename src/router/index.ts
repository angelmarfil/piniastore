import { createRouter, createWebHistory } from 'vue-router'
import DashboardView from '@/views/DashboardView.vue'
import StudentsView from '@/views/StudentsView.vue'
import StudentView from '@/views/StudentView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: DashboardView
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/estudiantes',
      name: 'students',
      component: StudentsView
    },
    {
      path: '/estudiantes/:id',
      name: 'student',
      component: StudentView
    }
  ]
})

export default router
